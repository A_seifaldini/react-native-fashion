import React from "react";
import { Theme, Box, Text } from "../../components/Theme";
import { RectButton } from "react-native-gesture-handler";
import { RoundedIcon } from "../../components";

export interface DrawerItemProps {
  icon: string;
  label: string;
  screen: string;
  color: keyof Theme["colors"];
}

export default ({ icon, label, screen, color }: DrawerItemProps) => {
  return (
    <RectButton>
      <Box flexDirection="row" alignItems="center" padding="m" borderRadius="m">
        <RoundedIcon
          name={icon}
          size={36}
          backgroundColor={color}
          color="white"
        />
        <Text variant="button" color="secondary" marginLeft="m">{label}</Text>
      </Box>
    </RectButton>
  );
};
