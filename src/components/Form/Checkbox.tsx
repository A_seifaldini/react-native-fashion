import React from "react";
import { Feather as Icon } from "@expo/vector-icons";
import { TouchableOpacity } from "react-native";

import { Box, Text } from "..";

interface CheckboxProps {
  label: string;
  checked: boolean;
  onChange: () => void;
}

export default ({ label, checked, onChange }: CheckboxProps) => {
  return (
    <TouchableOpacity
      onPress={() => onChange()}
      style={{ justifyContent: "center" }}
    >
      <Box flexDirection="row" alignItems="center">
        <Box
          borderRadius="s"
          backgroundColor={checked ? "primary" : "white"}
          justifyContent="center"
          alignItems="center"
          height={20}
          width={20}
          borderWidth={1}
          borderColor="primary"
          marginRight="m"
        >
          <Icon name="check" color="white" />
        </Box>
        <Text>{label}</Text>
      </Box>
    </TouchableOpacity>
  );
};
