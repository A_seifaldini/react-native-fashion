import React, { forwardRef } from "react";
import { Feather as Icon } from "@expo/vector-icons";
import {
  TextInput as RNTextInput,
  StyleSheet,
  TextInputProps as RNTextInputProps,
} from "react-native";

import { Box, useTheme } from "../Theme";
import { RoundedIcon } from "..";

interface TextInputProps extends RNTextInputProps {
  icon: string;
  touched?: boolean;
  error?: string;
}

export default forwardRef<RNTextInput, TextInputProps>(
  ({ icon, touched, error, ...props }, ref) => {
    const theme = useTheme();

    const SIZE = theme.borderRadii.m * 2.5;

    const reColor = !touched ? "text" : error ? "danger" : "primary";
    const color = theme.colors[reColor];

    return (
      <Box
        flexDirection="row"
        height={48}
        alignItems="center"
        borderRadius="s"
        borderColor={reColor}
        borderWidth={StyleSheet.hairlineWidth * 2}
      >
        <Box padding="s">
          <Icon name={icon} size={16} {...{ color }} />
        </Box>
        <Box flex={1}>
          <RNTextInput
            ref={ref}
            underlineColorAndroid="transparent"
            placeholderTextColor={color}
            {...props}
          />
        </Box>
        {touched && (
          <RoundedIcon
            name={!error ? "check" : "x"}
            size={SIZE}
            color="white"
            backgroundColor={!error ? "primary" : "danger"}
          />
          // <Box
          //   height={SIZE}
          //   width={SIZE}
          //   borderRadius="m"
          //   backgroundColor={!error ? "primary" : "danger"}
          //   justifyContent="center"
          //   alignItems="center"
          //   marginRight="s"
          //   style={{ borderRadius: SIZE / 2 }}
          // >
          //   <Icon
          //     name={!error ? "check" : "x"}
          //     color="white"
          //     style={{ textAlign: "center" }}
          //   />
          // </Box>
        )}
      </Box>
    );
  }
);
