import React from "react";

import { AuthNavigationProps } from "../components/Navigation";
import {
  Container,
  Text,
  Box,
  Button,
  RoundedIconButton,
  RoundedIcon,
} from "../components";

const SIZE = 80;

export default ({ navigation }: AuthNavigationProps<"PasswordChanged">) => {
  return (
    <Container
      footer={
        <Box flexDirection="row" justifyContent="center" marginBottom="s">
          <RoundedIconButton
            name="x"
            backgroundColor="white"
            color="secondary"
            size={60}
            onPress={() => navigation.pop()}
          />
        </Box>
      }
    >
      <Box alignSelf="center">
        <RoundedIcon
          name="check"
          size={SIZE}
          backgroundColor="primaryLight"
          color="primary"
        />
      </Box>
      <Text variant="title1" textAlign="center" marginBottom="l">
        Your password was succesfully changed
      </Text>
      <Text variant="body" textAlign="center" marginBottom="l">
        Close this window and login again
      </Text>
      <Box alignItems="center" marginTop="m">
        <Button
          variant="primary"
          onPress={() => navigation.navigate("Login")}
          label="Login again"
        />
      </Box>
    </Container>
  );
};
